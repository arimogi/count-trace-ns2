#!/usr/bin/perl

use warnings;
use strict;
use Text::ParseWords;
use Fcntl qw(SEEK_SET);

#===========================================================
# Check the arguments
#===========================================================
#printf( "$#ARGV \n" );
if ( $#ARGV == -1 ) {
	printf("\nError: No trace file found.");
	printf("\nUsage: $0 <trace-file-name> <packet-size> <time-of-simulation>\n\n");
	exit 1;
} else {
	if ( $#ARGV == 0 ) {
		printf("\nError: No packet size and no time of simulation.");
		printf("\nUsage: $0 <trace-file-name> <packet-size> <time-of-simulation>\n\n");
		exit 1;
	} elsif ( $#ARGV == 1 ) {
		printf("\nError: No time of simulation");
		printf("\nUsage: $0 <trace-file-name> <packet-size> <time-of-simulation>\n\n");
		exit 1;
	}
}

#===========================================================
# Open the trace file
#===========================================================
printf("====================================\n");
printf("Trace File: $ARGV[0] \n");
printf("====================================\n");
open(Trace, $ARGV[0]) or die "Error: Cannot open the trace file";

#===========================================================
# Initial Value
#===========================================================
my $sc = 0; # sending counter
my $rc = 0; # receiving counter
my $rp = 0;
my $mc =0;
my %pkt_fc = (); #packet forwarding counter

my ($enqueue_time) = 0;
my ($receive_time) = 0;
my ($packet_id) = 0;
my ($delay) = 0;
my ($total_receive_count) = 0;
my ($sum_of_delay) = 0;
my ($average_delay) = 0;
my ($simulation_time) = 0;
my ($file_position) = 0;
my (@line);

my $recvdSize = 0;
my $startTime = $ARGV[2]; # End time simulation
my $stopTime = 0;
my $hdr_size = 0;

my $temp_rc =0;
my $pkt = 0;
my $throughput = 0;

while (<Trace>) { # read one line in from the file
	@line = split; #split the line with delimin as space
	
	#------------------------------------------------------------------------------
	# PDR Counting
	#------------------------------------------------------------------------------
	if ($line[3] eq "AGT" && $line[6] eq "cbr") { # an application agent trace line
		if ($line[0] eq "s") { # a packet sent by some data source
			$sc++;
			$pkt_fc{$line[5]} = 0; #define the forwarding couter for this pkt
		}

		if ($line[0] eq "r") { # a packet received by sink
			$rc++;
			$pkt_fc{$line[5]}++; # one more hop to complement
		}
	}

	if ($line[3] eq "MAC" && $line[6] eq "cbr") {
		if ($line[0] eq "s") { 
			$mc++
		}
	}
	
	if ($line[3] eq "RTR") { # a routing agent trace lineOD
		if($line[0] eq "f") {
			$rp++;
			$pkt_fc{$line[5]}++;
		}
	}
	
	#------------------------------------------------------------------------------
	# End to End Delay Counting
	#------------------------------------------------------------------------------
	if (($line[0] eq 's') && ($line[3] eq 'AGT')) {
		$file_position = tell(Trace);
		$enqueue_time = $line[1];
		$packet_id = $line[5];
		while (<Trace>) {
			@line = split(' ');
			if (($line[0] eq 'r') && ($line[3] eq 'AGT')) {
				if (($line[5] == $packet_id)) {
					$receive_time = $line[1];
					$total_receive_count++;
					$delay = $receive_time - $enqueue_time;
					$sum_of_delay = $sum_of_delay + $delay;

					#Following is for debug.
					#$delay = $delay * 1000;
					#print("\nDelay= $delay");

					last;
				}
			}
		}
		#Continue to search for next 's' event from where the previous 's' was found.
		#So move to the same line where previous 's' event was found.
	seek(Trace, $file_position, SEEK_SET);
	}
	
	#------------------------------------------------------------------------------
	# Troughput Counting
	#------------------------------------------------------------------------------
	if ( $line[3] eq "AGT" and $line[0] eq "s" and $line[7] >= $ARGV[1] ) {
		if ( $line[1] < $startTime ) {
			$startTime = $line[1];
		}
	}

	# Update total received packets' size and store packets arrival time
	if ( $line[3] eq "AGT" and $line[0] eq "r" and $line[7] >= $ARGV[1] ) {
		if ( $line[1] > $stopTime ) {
			$stopTime = $line[1];
		}

		# Rip off the header
		$hdr_size = $line[7] % 512;
		$line[7] -= $hdr_size;
		# Store received packet's size
		$recvdSize += $line[7];
	}
}
$simulation_time = $line[1];
close(Trace); #close the file

$throughput = eval {($recvdSize/($stopTime-$startTime))*(8/1000)};

foreach $pkt ( keys %pkt_fc ) {
	$temp_rc += $pkt_fc{$pkt}; #the total forwarding times
}

if ($rc > 0) {
	printf("Simulation Time = %1.3f seconds\n", $simulation_time);	
	printf("Sent packets = %d\n", $sc);
	printf("Total Receive Count = %1.3f\n", $total_receive_count);
	#printf("Received packets = %d\n", $rc);
	printf("Packets dropped = %d\n", $sc-$rc);
	printf("Routing agents = %d\n", $rp);
	printf("Routing overhead = %1.3f % \n", $rp/$sc*100 );
	printf("Packet Delivery Ratio = %1.3f % \n", $rc/$sc*100 );
	printf("Average Path Length = %1.3f\n", $temp_rc/$rc);

	if ( $total_receive_count != 0 ) {
		$average_delay = $sum_of_delay / $total_receive_count;
		$average_delay = $average_delay * 1000;
		printf("Average End to End Delay = %1.3f ms\n", $average_delay);
	} else {
		print("No packet received.\n");
	}

	printf("Average Throughput = %1.3f kbps\n", $throughput);
}
